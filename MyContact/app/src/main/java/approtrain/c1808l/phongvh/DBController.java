package approtrain.c1808l.phongvh;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DBController {
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Cursor mCursor;
    private MyDBHelper myDBHelper;
    private SQLiteDatabase mDatabase;

    public DBController(Context context) {
        myDBHelper = new MyDBHelper(context);
        mDatabase = myDBHelper.getWritableDatabase();
        getCursor();
        addDefaultList();
    }

    public void insert(Contact contact) {
        ContentValues values = new ContentValues();
        values.put(Contract.COLUMN_NAME, contact.getName());
        values.put(Contract.COLUMN_DATE, dateFormat.format(contact.getDate()));
        values.put(Contract.COLUMN_IMPORTANT, contact.isImportant());
        mCursor.moveToLast();
        contact.setId(mCursor.getPosition());

        mDatabase.insert(Contract.TABLE_NAME, null, values);
    }

    public void delete(int id) {
        mDatabase.delete(Contract.TABLE_NAME, Contract.COLUMN_ID + " = " + id, null);
    }

    public void update(int id, Contact contact) {
        ContentValues values = new ContentValues();
        values.put(Contract.COLUMN_NAME, contact.getName());
        values.put(Contract.COLUMN_IMPORTANT, contact.isImportant());

        mDatabase.update(Contract.TABLE_NAME, values, Contract.COLUMN_ID + " = " + id, null);
    }

    public void addDefaultList() {
        if (getItemCount() < 1) {
            insert(new Contact("Contact 1", new Date(), true));
            insert(new Contact("Contact 2", new Date(), true));
            insert(new Contact("Contact 3", new Date(), false));
            insert(new Contact("Contact 4", new Date(), true));
        }
    }

    public ArrayList<Contact> getListContact() throws ParseException {
        ArrayList<Contact> arrayList = new ArrayList<>();
        mCursor.moveToFirst();
        while (!mCursor.isAfterLast()) {
            Contact contact = new Contact();
            contact.setId(mCursor.getInt(mCursor.getColumnIndex(Contract.COLUMN_ID)));
            contact.setName(mCursor.getString(mCursor.getColumnIndex(Contract.COLUMN_NAME)));
            contact.setDate(dateFormat.parse(mCursor.getString(mCursor.getColumnIndex(Contract.COLUMN_DATE))));
            contact.setImportant(mCursor.getInt(mCursor.getColumnIndex(Contract.COLUMN_IMPORTANT)) > 0);
            arrayList.add(contact);
            mCursor.moveToNext();
        }
        return arrayList;
    }

    public Cursor getCursor() {
        mCursor = mDatabase.rawQuery("select * from " + Contract.TABLE_NAME, null);
        return mCursor;
    }

    public int getItemCount() {
        return mCursor.getCount();
    }

}
