package approtrain.c1808l.phongvh;

import java.io.Serializable;
import java.util.Date;

public class Contact implements Serializable {
    private int id;
    private String name;
    private Date date;
    private boolean isImportant;

    public Contact() {
    }

    public Contact(String name, Date date, boolean isImportant) {
        this.name = name;
        this.date = date;
        this.isImportant = isImportant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isImportant() {
        return isImportant;
    }

    public void setImportant(boolean important) {
        isImportant = important;
    }


}
