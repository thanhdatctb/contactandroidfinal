package approtrain.c1808l.phongvh;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {
    ListView lvContact;
    CustomCursorAdapter adapter;
    DBController dbController;
    ArrayList<Contact> contactArrayList;
    AlertDialog alertDialog;
    AlertDialog.Builder builder;
    Cursor cursor;
    EditText edtName;
    CheckBox cbImportant;
    View content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvContact = (ListView) findViewById(R.id.lvContact);
        dbController = new DBController(this);
        content = LayoutInflater.from(this).inflate(R.layout.dialog_content, null, false);
        edtName = (EditText) content.findViewById(R.id.edt_name);
        cbImportant = (CheckBox) content.findViewById(R.id.cb_important);
        cursor = dbController.getCursor();
        adapter = new CustomCursorAdapter(this, cursor, false);
        lvContact.setAdapter(adapter);
        registerForContextMenu(lvContact);
        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    contactArrayList = dbController.getListContact();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("contact", contactArrayList.get(position));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        getMenuInflater().inflate(R.menu.context_menu, menu);
        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int position = info.position;
        try {
            contactArrayList = dbController.getListContact();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        switch (item.getItemId()) {
            case R.id.item_edit:
                openEditDialog(contactArrayList.get(position));
                return true;
            case R.id.item_delete:
                dbController.delete(contactArrayList.get(position).getId());
                cursor.requery();
                adapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }

    }

    private void openEditDialog(Contact contact) {
        if (content.getParent() != null) {
            ((ViewGroup) content.getParent()).removeView(content);
        }
        content.setBackgroundColor(Color.BLUE);
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit contact");
        edtName.setText(contact.getName());
        cbImportant.setChecked(contact.isImportant());
        builder.setView(content);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact updateContact = new Contact();
                updateContact.setName(edtName.getText().toString());
                updateContact.setImportant(cbImportant.isChecked());
                dbController.update(contact.getId(), updateContact);
                cursor.requery();
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_add:
                openAddDialog();
                return true;
            case R.id.item_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openAddDialog() {
        if (content.getParent() != null) {
            ((ViewGroup) content.getParent()).removeView(content);
        }
        content.setBackgroundColor(Color.GREEN);
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Add contact");
        edtName.setText("");
        cbImportant.setChecked(false);
        builder.setView(content);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Contact contact = new Contact();
                contact.setName(edtName.getText().toString());
                contact.setImportant(cbImportant.isChecked());
                contact.setDate(new Date());
                dbController.insert(contact);
                cursor.requery();
                adapter.notifyDataSetChanged();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
}