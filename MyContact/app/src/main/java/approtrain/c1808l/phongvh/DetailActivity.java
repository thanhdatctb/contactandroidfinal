package approtrain.c1808l.phongvh;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity {
    TextView tvId, tvName, tvDate, tvImportant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        tvId = (TextView) findViewById(R.id.tv_detail_id);
        tvName = (TextView) findViewById(R.id.tv_detail_name);
        tvDate = (TextView) findViewById(R.id.tv_detail_date);
        tvImportant = (TextView) findViewById(R.id.tv_detail_important);
        Contact contact = (Contact) getIntent().getSerializableExtra("contact");

        tvId.setText(String.valueOf(contact.getId()));
        tvName.setText(contact.getName());
        tvDate.setText(contact.getDate().toString());
        tvImportant.setText(contact.isImportant() ? "Yes" : "No");


    }
}