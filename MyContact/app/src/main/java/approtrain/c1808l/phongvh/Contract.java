package approtrain.c1808l.phongvh;

public class Contract {
    public static final String DB_NAME = "contact.db";
    public static final int DB_VERSION = 1;
    public static final String TABLE_NAME = "table_contact";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_DATE = "date";
    public static final String COLUMN_IMPORTANT = "important";
}
