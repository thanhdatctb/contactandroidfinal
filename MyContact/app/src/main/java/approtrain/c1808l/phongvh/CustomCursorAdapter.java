package approtrain.c1808l.phongvh;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class CustomCursorAdapter extends CursorAdapter {
    public CustomCursorAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        View marker = view.findViewById(R.id.marker);
        TextView tvName = (TextView) view.findViewById(R.id.tv_name);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date);
        int important = cursor.getInt(cursor.getColumnIndex(Contract.COLUMN_IMPORTANT));
        String name = cursor.getString(cursor.getColumnIndex(Contract.COLUMN_NAME));
        String date = cursor.getString(cursor.getColumnIndex(Contract.COLUMN_DATE));
        marker.setBackgroundColor(important > 0 ? Color.RED : Color.GREEN);
        tvName.setText(name);
        tvDate.setText(date);
    }
}
